<?php

namespace Drupal\views_database_connector\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\Standard;
use Drupal\views\ResultRow;

/**
 * Extends the default implementation of the base field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("standard_vdc")
 */
class StandardVDC extends Standard {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['render_html'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['render_html'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Render the field as HTML'),
      '#description' => $this->t('Checking this box will render the field as HTML. Although the HTML will be sanitized using Drupal\'s "xss" filter, checking this box is not recommended unless you trust the source.'),
      '#default_value' => $this->options['render_html'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (isset($this->options['render_html']) && $this->options['render_html']) {
      return $this->sanitizeValue($this->getValue($values), 'xss');
    }
    else {
      return $this->sanitizeValue($this->getValue($values));
    }
  }

}
